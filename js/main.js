// Модальное окно

// открыть по кнопке
$('.js-button-campaign').click(function() { 
	$('.js-overlay-campaign').fadeIn();
});

// закрыть на крестик
$('.js-close-campaign').click(function() { 
	$('.js-overlay-campaign').fadeOut();
	
});

// закрыть по клику вне окна
$(document).mousedown(function (e) { 
	var popup = $('.js-popup-campaign');
	if (e.target!=popup[0]&&popup.has(e.target).length === 0){
		$('.js-overlay-campaign').fadeOut();
	}
});

// registr

$(".registr").click(function() {
	$('.js-overlay-campaign').fadeOut();
	$('.js-overlay-registr').fadeIn();
});

$('.js-close-campaign').click(function() { 
	$('.js-overlay-registr').fadeOut();
	
});

$(document).mousedown(function (e) { 
	var popup = $('.js-popup-campaign');
	if (e.target!=popup[0]&&popup.has(e.target).length === 0){
		$('.js-overlay-registr').fadeOut();
	}
});


$(function(){
	$(".js-show").mouseenter(function(){
		$(".js-img").fadeIn(1000);
	});
});

$(function(){
	$(".js-show").mouseleave(function(){
		$(".js-img").delay(500).fadeOut(1000);
	});
});


// show part of document

$('#main-part').click(function() {
    $('.main-part').removeClass("dnone");
    $('.statut-part').addClass("dnone");
    $('.decision-part').addClass("dnone");
    $('.protocols-part').addClass("dnone");
});
$('#statut-part').click(function() {
    $('.main-part').addClass("dnone");
    $('.statut-part').removeClass("dnone");
    $('.decision-part').addClass("dnone");
    $('.protocols-part').addClass("dnone");
});
$('#decision-part').click(function() {
    $('.main-part').addClass("dnone");
    $('.statut-part').addClass("dnone");
    $('.decision-part').removeClass("dnone");
    $('.protocols-part').addClass("dnone");
});
$('#protocols-part').click(function() {
    $('.main-part').addClass("dnone");
    $('.statut-part').addClass("dnone");
    $('.decision-part').addClass("dnone");
    $('.protocols-part').removeClass("dnone");
});
