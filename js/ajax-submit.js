$( "#do_signup" ).click(function() 
{
	var login = $('[name="login"]').val();
	var password = $('[name="password"]').val();

	if (login == "") {
		$("#popup-result").text('Заповніть поле Логін');
	} else if (password == "") {
		$("#popup-result").text('Заповніть поле Пароль');
	} else {

		$.post( "../libs/submit.php", 
		{
			login: login,
			password: password
		},
		function( data ) {
			$( "#popup-result" ).html( data );
		});
	}
});

$( "#do_reg" ).click(function() 
{

	var reg_login = $('[name="reg_login"]').val();
	var reg_password1 = $('[name="reg_password1"]').val();
	var reg_password2 = $('[name="reg_password2"]').val();

	if (reg_login == "") {
		$("#popup-result-reg").text('Заповніть поле Логін');
	} else if (reg_password1 == "") {
		$("#popup-result-reg").text('Заповніть поле Пароль');
	} else if (reg_password2 == "") {
		$("#popup-result-reg").text('Заповніть поле Пароль ще раз');
	} else if (reg_password1 != reg_password2) {
		$("#popup-result-reg").text('Паролі не збігаються!');
	} else {

		$.post( "../libs/registr.php", 
		{
			reg_login: reg_login,
			reg_password1: reg_password1,
			reg_password2: reg_password2
		},
		function( data ) {
			$( "#popup-result-reg" ).html( data );
		});

	}
	
});

$( "#search" ).click(function() {

	var search = $('#apart').val();

	if (search != "") {

		$.post( "../libs/payment.php",
		{
			search: search
		},
		function( data ) {
			$("#search_results").html(data);
		});
	}

});

