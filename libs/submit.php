<?php

	require "connect.php";

	$login_error = array();
	$close_success = '<script> $(".js-overlay-campaign").fadeOut(); $(function() { $(".button").attr("style", "display: none;"); } ); </script>';

	$user = R::findOne('users', 'login = ?', array($data['login']));

	if ($user)
	{
		// Логін існує
		if(password_verify($data['password'], $user->password))
		{
			// Все добре, логіним Користувача
			$user = $user->export();
			$_SESSION['logged_user'] = $user;
			print_r($close_success);
		} else {
			$login_error[] = 'Невірно введений Пароль';
		}
	} else {
		$login_error[] = 'Невірно введений Логін';
	}

	if (!empty( $login_error)) {
		echo  array_shift($login_error);	
	} else {
		print_r($close_success);
	}

	$admin_btn = '<script> $(function() { $(".admin_btn").attr("style" ,"display: block;") } ); </script>';

	$all_admin = R::findAll( 'admins' );

	foreach ($all_admin as $admin) {
		$admins = $admin->login;
	}

	if ($admins == $user['login']) {
		echo($admin_btn);
	}