<?php

	require "connect.php";

	$resident_db = R::find('residents', 'apartment = ?', array($data['search']));

	if (empty($resident_db)) {
		echo 'Такої квартири не існує!';
	} else {

		foreach ($resident_db as $res) {
			$resident_id = $res['id'];
		}

		$now_year = date ('Y');
		$prev_year = $now_year - 1;
		
		$payment_log_now = R::getAll( 'SELECT * FROM `payments` WHERE `resident_id` =  ? AND `year` = ? ORDER BY `id` DESC LIMIT 12', array($resident_id, $now_year) );
		$payment_log_prev = R::getAll( 'SELECT * FROM `payments` WHERE `resident_id` =  ? AND `year` = ? ORDER BY `id` DESC LIMIT 4', array($resident_id, $prev_year) );
		$overpayment_log = R::getAll( 'SELECT `overpayment` FROM `payments` WHERE `resident_id` = ? AND `year` = ? ORDER BY `id` DESC LIMIT 1', array($resident_id, $now_year) );

		foreach ($overpayment_log as $overpay) {
			$pay_m = $overpay['overpayment'];
		}

		if ($pay_m > 0) {
			$main_pay = '<span style="color: #B01F1F;">Заборгованість: '.$pay_m.'<span>';
		} else if ($pay_m == 0) {
			$main_pay = '<span style="color: #111B24;">Заборгованість відсутня<span>';
		} else {
			$main_pay = '<span style="color: #4C9F52;">Переплата: '.$pay_m.'<span>';
		}

		echo '<section class="result_search">';

			echo ' <span class="id">Номер квартири: '.$data['search'].'</span>
			<section class="now_year_res">
			<span class="year">'.$now_year.' рік</span>
				<section class="main_payment">'.$main_pay.'</section>
				<table class="info_table" cellspacing="0">

					<tr class="tblock_main">
						<th>Місяць</th>
						<th>Нарахов.</th>
						<th>Субсидія</th>
						<th>Пільги</th>
						<th>Оплата</th>
						<th>Заборг.(+) Перепл.(-)</th>
					</tr>';

					if (empty($payment_log_now)) {
						echo '<tr class="tblock_wrapper">
									<td>Січень</td>
									<td>0.00</td>
									<td>0.00</td>
									<td>0.00</td>
									<td>0.00</td>
									<td>0.00</td>
								</tr>';
					} else {
						foreach ($payment_log_now as $pay_now) {
						$pay_res_n = $pay_now['overpayment'];
						if ($pay_res_n > 0) {
							$pay_n = '<span style="color: #B01F1F; font-size: 18px; font-weight: 400;">'.$pay_res_n.'<span>';
						} else {
							$pay_n = '<span style="color: #4C9F52; font-size: 18px; font-weight: 400;">'.$pay_res_n.'<span>';
						}
						$year = $pay_now['year'];
						echo '<tr class="tblock_wrapper">
									<td>'.$pay_now['month'].'</td>
									<td>'.$pay_now['accrued'].'</td>
									<td>'.$pay_now['subsidy'].'</td>
									<td>'.$pay_now['privileges'].'</td>
									<td>'.$pay_now['payment'].'</td>
									<td id="pay">'.$pay_n.'</td>
								</tr>';
						}
						unset($pay_now);
					}

				echo '</table></section>';

			echo '<section class="prev_year_res">
					<span class="year">'.$prev_year.' рік</span>

						<table class="info_table" cellspacing="0">

							<tr class="tblock_main">
								<th>Місяць</th>
								<th>Нарахов.</th>
								<th>Субсидія</th>
								<th>Пільги</th>
								<th>Оплата</th>
								<th>Заборг.(+)<br>Перепл.(-)</th>
							</tr>';

							if (empty($payment_log_prev)) {

								echo '<tr class="tblock_wrapper">
											<td>Грудень</td>
											<td>0.00</td>
											<td>0.00</td>
											<td>0.00</td>
											<td>0.00</td>
											<td>0.00</td>
										</tr>';
								
							} else {

								foreach ($payment_log_prev as $pay_prev) {

								$pay_res_p = $pay_now['arrears'] - $pay_now['overpayment'];
								if ($pay_res < 0) {
									$pay_p = '<span style="color: #B01F1F; font-size: 18px; font-weight: 400;">'.$pay_res_p.'<span>';
								} else {
									$pay_p = '<span style="color: #4C9F52; font-size: 18px; font-weight: 400;">'.$pay_res_p.'<span>';
								}

								echo '<tr class="tblock_wrapper">
											<td>'.$pay_prev['month'].'</td>
											<td>'.$pay_prev['accrued'].'</td>
											<td>'.$pay_prev['subsidy'].'</td>
											<td>'.$pay_prev['privileges'].'</td>
											<td>'.$pay_prev['payment'].'</td>
											<td>'.$pay_p.'</td>
										</tr>';
								};
								unset($pay_prev);

							}

							

					echo '</table></section>';

		echo '</section>';
	}
	


