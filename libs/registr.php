<?php 
	
	require "connect.php";

	$reg_error = array();
	$close_success_reg = '<script> $(".js-overlay-registr").fadeOut(); </script>';


	$usermay = R::findOne('users', 'login = ?', array($data['reg_login']));

	if (!$usermay) {
		// all right reg is become
		$users = R::dispense('users');

		$users->login = $data['reg_login'];
		$users->password = password_hash($data['reg_password1'], PASSWORD_DEFAULT);

		R::store($users);

	} else {
		$reg_error[] = 'Цей Логін зайнятий!';
	}

	if (!empty( $reg_error)) {
		echo  array_shift($reg_error);	
	} else {
		print_r($close_success_reg);
	}
