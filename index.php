<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>ОСББ «Поділля 2015»</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	 <meta name="description" content="Головна сторінка ОСББ Поділля 2015 Цей веб-сайт спеціалізується на пошуку інформації про платіжки клієнтів ОСББ Поділля 2015. Цей сервіс зручний та швидкий через те, що Ви буквально у два кліки дізнаєтесь всю інформацію яка вас цікавить." />
	 <meta name="keywords" content="Комуналка, ОСББ, Поділля 2015, Організація Співмешканців Багатоквартирних Будинків" />
	<meta name="author" content="Eugene Budzinskiy" />
	<link rel="shortcut icon" href="image/favicon.png">
	<link rel="stylesheet" href="css/style.css" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Marck+Script&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400&amp;subset=cyrillic" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:700&amp;subset=cyrillic" rel="stylesheet">
</head>
<body>
	<section class="main_page">
		<settion class="header">
			<section class="primary_menu red" id="main_offset">
				<span class="logo_text"><a href="/">КП</a></span>
				<?php include 'pages/nav-menu.php'; ?>
				<section class="slogan_wrapper">
					<span class="title_slogan">Шукай свої платіжки тут!</span>
					<span class="second_slogan">Зручно, Швидко, Просто.</span>
					<a href="/pages/search.php"><button class="main_search">Пошук</button></a>
				</section>
			</section>
		</settion>
		<section class="content">
			<section class="main_text" id="main_offset">
				<span class="title_text">Зручно, Швидко, Просто</span>
				<span class="text">Цей веб-сайт спеціалізується на пошуку інформації про платіжки клієнтів ОСББ Поділля 2015. Цей сервіс зручний та швидкий через те, що Ви буквально у два кліки дізнаєтесь всю інформацію яка вас цікавить. А саме на рахунок заборгованості чи навпаки переплати послуг. Також простим цей сайт можна назвати через те, що тут не хитромудре управління, тут все просто і зрозуміло. Дані які Ви можете побачити на сайті, взяті з бази данних. База данних щодня оновлюється і коригується головним адміністратором. Дані дістаються безпосередньо з локального ОСББ. Кінцевий результат підраховується за встановленими тарифами на сьогоднішній день. Ось такий ланцюжок дані проходять перед тим як бути виведеними у користувача на дисплеї.  </span>
				<section class="colors">
					<div class="color1" style="background: #F74C55;"></div>
					<div class="color2" style="background: #E0DB3F;"></div>
					<div class="color3" style="background: #3D8EAC;"></div>
					<div class="color4" style="background: #404040;"></div>
					<div class="color5" style="background: #111B24;"></div>
				</section>
			</section>
			<section class="city_more" id="main_offset">
				<section class="city_wrapper">
					<span>Кам'янець-Подільський - прекрасне місто.</span>
					<a href="slider/index-slider.php"><button class="to_slider">Показати більше</button></a>
				</section>
			</section>
		</section>
		<section class="color_line" id="main_offset"></section>
		<?php include 'pages/footer.php'; ?>
	</section>
</body>
</html>