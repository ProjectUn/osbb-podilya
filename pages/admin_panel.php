<?php 
	require '../libs/connect.php';
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Адмін Панель «Поділля 2015»</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	<link rel="shortcut icon" href="../image/favicon.png"> 
	<link rel="stylesheet" href="../css/style.css" type="text/css">
	<link rel="stylesheet" href="../css/popup.css" type="text/css">
</head>
<body>
	<section class="main_page">
		<settion class="header">
			<section class="primary_menu_admin" id="main_offset">
				<span class="logo_text"><a href="/">КП</a></span>
				<?php include 'nav-menu.php'; ?>
			</section>
		</settion>
		<section class="content">
			<section class="main_text" id="main_offset">
				<?php

					$test_user = $_SESSION['logged_user']['login'];

					$all_admin = R::findAll( 'admins' );

					foreach ($all_admin as $admins) {
						$admin = $admins->login;
					}

					if ($admin == $test_user) : ?>
						<section class="add_panel">
							<form action="" method="POST" class="add_form">
								
								

							</form>
						</section>
					<?php else : ?>
						<span class="not_permission">У вас відсутні права на користування данною сторінкою!</span>
						<span class="not_permission">Будь-ласка покиньте цю сторінку.</span>
					<?php endif; ?>
			</section>
		</section>
		<section class="color_line" id="main_offset"></section>
		<?php include 'footer.php'; ?>
	</section>
<script src="../js/jquery-3.2.1.min.js"></script>
<script src="../js/ajax-submit.js"></script>
<script src="../js/main.js"></script>
</body>
</html>