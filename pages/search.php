<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Пошук «Поділля 2015»</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	 <meta name="description" content="Платіжки ОСББ Поділля 2015" />
	 <meta name="keywords" content="Комуналка, ОСББ, Поділля 2015, Платіжки, Скільки за комуналку" />
	<meta name="author" content="Eugene Budzinskiy" />
	<link rel="shortcut icon" href="../image/favicon.png"> 
	<link rel="stylesheet" href="../css/style.css" type="text/css">
	<link rel="stylesheet" href="../css/popup.css" type="text/css">
</head>
<body>
	<section class="main_page">
		<settion class="header">
			<section class="primary_menu s1 js-show" id="main_offset">
				<span class="logo_text"><a href="/">КП</a></span>
				<?php include 'nav-menu-h.php';?>
				<div class="wallpaper-block">
					<img src="../image/wallpaper/10.jpg" alt="" class="wallpaper js-img">
					<img src="../image/wallpaper/20.jpg" alt="" class="wallpaper js-img">
				</div>
				<section class="vindex">
				    <?php include 'signup.php';?>
                </section>
			</section>
		</settion>
		<section class="content" id="main_offset">
			<form class="search" method="POST" action="">
				<span class="title">Переглянути стан оплати послуг</span>
				<span class="subtitle">Введіть номер квартири:</span>
				<input type="text" maxlength="3" class="search_input" id="apart">
				<button type="button" class="search_button" id="search">Пошук</button>
				<span id="search_error"></span>
			</form>
			<section id="search_results"></section>
		</section>
		<section class="color_line" id="main_offset"></section>
		<?php include 'footer.php'; ?>
	</section>
<script src="../js/jquery-3.2.1.min.js"></script>
<script src="../js/ajax-submit.js"></script>
<script src="../js/main.js"></script>
</body>
</html>