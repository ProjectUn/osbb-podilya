<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Контакти «Поділля 2015»</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	 <meta name="description" content="Контакти ОСББ Поділля 2015" />
	 <meta name="keywords" content="Контакти, ОСББ, Поділля 2015, Голова ОСББ" />
	<meta name="author" content="Eugene Budzinskiy" />
	<link rel="shortcut icon" href="../image/favicon.png"> 
	<link rel="stylesheet" href="../css/style.css" type="text/css">
	<link rel="stylesheet" href="../css/popup.css" type="text/css">
</head>
<body>
	<section class="main_page">
		<settion class="header">
			<section class="primary_menu s1 js-show" id="main_offset">
				<span class="logo_text"><a href="/">КП</a></span>
				<?php include 'nav-menu-h.php';?>
				<div class="wallpaper-block">
					<img src="../image/wallpaper/50.jpg" alt="" class="wallpaper js-img">
					<img src="../image/wallpaper/60.jpg" alt="" class="wallpaper js-img">
				</div>
				<?php include 'signup.php';?>
			</section>
		</settion>
		<section class="content">
			<section class="main_text" id="main_offset">
				<section class="contacts">
					<span>Контакти</span>
					<div class="cont_block">
						<span class="cont_title">Голова ОСББ "Поділля 2015" - Григоришена Валентина Іванівна</span>
						<span class="cont_info">Телефон: +380962070735</span>
					</div><br>
					<div class="cont_block">
						<span class="cont_title">Бухгалтер - Дрозд Віталій Іванович</span>
						<span class="cont_info"> Телефон: +380678124023</span>
					</div><br>
					<div class="cont_block">
						<span class="cont_title">Менеджер в/о голови - Полюляк Ольга Іванівна</span>
						<span class="cont_info"> Телефон: +380970267676</span>
					</div><br>
					<div class="cont_block">
						<span class="cont_title">Електрик - Грондецький Геннадій</span>
						<span class="cont_info"> Телефон: +380960962456</span>
					</div><br>
					<div class="cont_block">
						<span class="cont_title">Двірник - Матвієнко Дмитро Олексійович</span>
						<span class="cont_info"> Телефон: +380987174795</span>
					</div>
				</section>
				<section class="google_map">
					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d1720.3577277744087!2d26.567425251014413!3d48.70133127659585!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e1!3m2!1sru!2sua!4v1512570988246" width="515" height="400" frameborder="0" style="border:0" allowfullscreen></iframe>
				</section>
			</section>
		</section>
		<section class="color_line" id="main_offset"></section>
		<?php include 'footer.php'; ?>
	</section>
<script src="../js/jquery-3.2.1.min.js"></script>
<script src="../js/ajax-submit.js"></script>
<script src="../js/main.js"></script>
</body>
</html>