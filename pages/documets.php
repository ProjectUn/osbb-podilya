<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Контакти «Поділля 2015»</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	 <meta name="description" content="Контакти ОСББ Поділля 2015" />
	 <meta name="keywords" content="Контакти, ОСББ, Поділля 2015, Голова ОСББ" />
	<meta name="author" content="Eugene Budzinskiy" />
	<link rel="shortcut icon" href="../image/favicon.png"> 
	<link rel="stylesheet" href="../css/style.css" type="text/css">
	<link rel="stylesheet" href="../css/popup.css" type="text/css">
</head>
<body>
	<section class="main_page">
		<settion class="header">
			<section class="primary_menu s1 js-show" id="main_offset">
				<span class="logo_text"><a href="/">КП</a></span>
				<?php include 'nav-menu-h.php';?>
				<div class="wallpaper-block">
					<img src="../image/wallpaper/30.jpg" alt="" class="wallpaper js-img">
					<img src="../image/wallpaper/40.jpg" alt="" class="wallpaper js-img">
				</div>
				<?php include 'signup.php';?>
			</section>
		</settion>
		<section class="content">
			<section class="main_text" id="main_offset_r">
				<section class="all_document">
                    <div class="left-sidebar">
                        <div class="body-sidebar">
                            <div class="side-link">
                                <button id="main-part" type="button">Головна</button>
                            </div>
                            <div class="side-link">
                                <button id="statut-part" type="button">Статут</button>
                            </div>
                            <div class="side-link">
                                <button id="decision-part" type="button">Рішення</button>
                            </div>
                            <div class="side-link">
                                <button id="protocols-part" type="button">Протоколи</button>
                            </div>
                        </div>
                    </div>
                    <div class="current-doc">
                        <div class="main-part">
                            <div class="part-title"><span>Головна</span></div>
                            <div class="documents">
                                <img src="../image/main/main01.png" alt="">
                            </div>
                        </div>
                        <div class="statut-part dnone">
                            <div class="part-title"><span>Статут</span></div>
                            <div class="documents">
                                <img src="../image/statut/doc01.png" alt="">
                                <img src="../image/statut/doc02.png" alt="">
                                <img src="../image/statut/doc03.png" alt="">
                                <img src="../image/statut/doc04.png" alt="">
                                <img src="../image/statut/doc05.png" alt="">
                                <img src="../image/statut/doc06.png" alt="">
                                <img src="../image/statut/doc07.png" alt="">
                                <img src="../image/statut/doc08.png" alt="">
                                <img src="../image/statut/doc09.png" alt="">
                                <img src="../image/statut/doc10.png" alt="">
                                <img src="../image/statut/doc11.png" alt="">
                                <img src="../image/statut/doc12.png" alt="">
                                <img src="../image/statut/doc13.png" alt="">
                            </div>
                        </div>
                        <div class="decision-part dnone">
                            <div class="part-title"><span>Рішення</span></div>
                            <div class="documents">
                                <img src="../image/decision/dec01.png" alt="">
                                <img src="../image/decision/dec02.png" alt="">
                                <img src="../image/decision/dec03.png" alt="">
                                <img src="../image/decision/dec04.png" alt="">
                                <img src="../image/decision/dec05.png" alt="">
                                <img src="../image/decision/dec06.png" alt="">
                                <img src="../image/decision/dec07.png" alt="">
                                <img src="../image/decision/dec08.png" alt="">
                            </div>
                        </div>
                        <div class="protocols-part dnone">
                            <div class="part-title"><span>Протоколи</span></div>
                            <div class="documents">
                                <img src="../image/protocol/prot01.png" alt="">
                                <img src="../image/protocol/prot02.png" alt="">
                                <img src="../image/protocol/prot03.png" alt="">
                                <img src="../image/protocol/prot04.png" alt="">
                                <img src="../image/protocol/prot05.png" alt="">
                                <img src="../image/protocol/prot06.png" alt="">
                                <img src="../image/protocol/prot07.png" alt="">
                                <img src="../image/protocol/prot08.png" alt="">
                                <img src="../image/protocol/prot09.png" alt="">
                                <img src="../image/protocol/prot10.png" alt="">
                            </div>
                        </div>
                    </div>
                </section>
			</section>
		</section>
		<section class="color_line" id="main_offset"></section>
		<?php include 'footer.php'; ?>
	</section>
<script src="../js/jquery-3.2.1.min.js"></script>
<script src="../js/ajax-submit.js"></script>
<script src="../js/main.js"></script>
</body>
</html>