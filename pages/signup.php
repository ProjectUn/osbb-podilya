<section class="popup_block">
	<main class="main">
		<div class="button js-button-campaign"><span>Вхід в систему</span></div>
		<a href="../pages/admin_panel.php"><div class="admin_btn" style="display: none;" >Admin Panel</div></a>
	</main>
	<div class="overlay js-overlay-campaign">
		<div class="popup js-popup-campaign">
			<section class="style-popup">
					<section class="head_popup">
						<span>Авторизація</span>
						<div class="close-popup js-close-campaign"></div>
					</section>
					<section class="body_popup">
						<div id="popup-result"></div>
						<form action="" method="POST">
							<input type="text" placeholder="Логін" name="login">
							<input type="password" placeholder="●●●●●●" name="password">
							<button id="do_signup" type="button">Увійти</button>
						</form>
						<p>Немає аккаунта? &nbsp;&nbsp;<span class="registr">Реєстрація</span></p>
					</section>
			</section>
		</div>
	</div>
	<div class="overlay-reg js-overlay-registr">
		<div class="popup js-popup-campaign">
			<section class="style-popup">
					<section class="head_popup">
						<span>Реєстрація</span>
						<div class="close-popup js-close-campaign"></div>
					</section>
					<section class="body_popup">
						<div id="popup-result-reg"></div>
						<form action="" method="POST">
							<input type="text" placeholder="Логін або Email" name="reg_login">
							<input type="password" placeholder="Пароль" name="reg_password1">
							<input type="password" placeholder="Пароль ще раз" name="reg_password2">
							<button id="do_reg" type="button">Зареєструватись</button>
						</form>
					</section>
			</section>
		</div>
	</div>
</section>


