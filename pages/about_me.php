<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Про Мене «Поділля 2015»</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	 <meta name="description" content="Про автора ОСББ Поділля 2015" />
	 <meta name="keywords" content="Eugene Budzinskiy, Євген Будзінський, Юджин Будзінський, Про автора" />
	<meta name="author" content="Eugene Budzinskiy" />
	<link rel="shortcut icon" href="../image/favicon.png"> 
	<link rel="stylesheet" href="../css/style.css" type="text/css">
</head>
<body>
	<section class="main_page">
		<settion class="header">
			<section class="primary_menu red" id="main_offset">
				<span class="logo_text"><a href="/">КП</a></span>
				<?php include 'nav-menu.php'; ?>
			</section>
		</settion>
		<section class="content">
			<section class="main_text" id="main_offset">
				<span class="title_text">Творець сайту - Eugene Budzinskiy</span>
				<span class="text">Цей проект є моєю спробою створити сервіс для місцевого ОСББ. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Provident quisquam, velit neque consequuntur deleniti! Eius nisi explicabo esse eum quia totam aliquid, rerum, est harum, fugit id. Repellendus eligendi, fugiat rerum repellat consequatur quia deleniti asperiores deserunt tempore reiciendis iusto, atque ducimus quasi explicabo fugit animi enim temporibus officia sed inventore ea minus, accusamus doloribus. Excepturi iure, mollitia odit, corporis ea maiores rem dicta ipsum pariatur quibusdam quaerat enim quae. Laborum nihil voluptate, necessitatibus voluptates culpa quos impedit libero obcaecati quia illo ab nisi ducimus, numquam odit dignissimos, recusandae, odio sapiente velit dolorem. Quidem, dolorum odit! Optio possimus modi tempora, reiciendis eligendi, illo. Quaerat quisquam, totam iure itaque eos repudiandae, ratione animi at pariatur impedit id unde dicta porro mollitia maiores ut iste accusamus est harum dolore in? Quidem quaerat dolorem ad sequi corporis, et sapiente tenetur distinctio voluptate natus! Ipsum nemo est incidunt iure libero veniam aspernatur, vero, officiis? Repudiandae id, minima, facilis voluptatem voluptatum perferendis tenetur minus accusantium architecto est nisi inventore impedit sed beatae alias quasi eum. Voluptatum totam voluptatibus itaque saepe, facilis est reprehenderit. Consequatur totam unde sint laudantium fugiat, voluptatum culpa soluta odit sapiente aliquid, libero ipsam corporis animi necessitatibus temporibus earum, labore ad repellendus?  </span>
				<section class="img_contacts">
					<a href="https://www.facebook.com/eugene.unusual" target="_blank"><img src="/image/facebook.png" alt=""></a>
					<a href="https://plus.google.com/113831189313484557386" target="_blank"><img src="/image/gmail.png" alt=""></a>
					<a href="https://twitter.com/Eugene_unusual" target="_blank"><img src="/image/twitter.png" alt=""></a>
				</section>
			</section>
		</section>
		<section class="color_line" id="main_offset"></section>
		<?php include 'footer.php'; ?>
	</section>
</body>
</html>