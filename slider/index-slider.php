<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Про Камянець-Подільський «Поділля 2015»</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"> 
	<meta name="viewport" content="width=device-width, initial-scale=1.0"> 
	 <meta name="description" content="Про Місто Кам'янець-Подільський" />
	 <meta name="keywords" content="Кам'янець-Подільський, Кам'янець Подільський, слайдер, опис міста" />
	<meta name="author" content="Eugene Budzinskiy" />
	<link rel="shortcut icon" href="../image/favicon.png"> 
   <link rel="stylesheet" type="text/css" href="css/demo.css" />
	<link rel="stylesheet" type="text/css" href="css/slicebox.css" />
	<link rel="stylesheet" type="text/css" href="css/custom.css" />
	<link rel="stylesheet" href="../css/style.css">
	<script type="text/javascript" src="js/modernizr.custom.46884.js"></script>
</head>
<body>
	
	<section class="main_page">
		
		<settion class="header">
			
			<section class="blue primary_menu" id="main_offset">
				
				<span class="logo_text"><a href="/">КП</a></span>

				<?php include '../pages/nav-menu.php'; ?>

				<section class="slider">
					
					<div class="container">

						<div class="wrapper">

							<ul id="sb-slider" class="sb-slider">
								<li>
									<img src="images/10.jpg" alt="image1"/>
								</li>
								<li>
									<img src="images/20.jpg" alt="image2"/>
								</li>
								<li>
									<img src="images/30.jpg" alt="image1"/>
								</li>
								<li>
									<img src="images/40.jpg" alt="image1"/>
								</li>
								<li>
									<img src="images/50.jpg" alt="image1"/>
								</li>
								<li>
									<img src="images/60.jpg" alt="image1"/>
								</li>
								<li>
									<img src="images/70.jpg" alt="image1"/>
								</li>
							</ul>

							<div id="shadow" class="shadow"></div>

							<div id="nav-arrows" class="nav-arrows">
								<a href="#">Next</a>
								<a href="#">Previous</a>
							</div>

							<div id="nav-dots" class="nav-dots">
								<span class="nav-dot-current"></span>
								<span></span>
								<span></span>
								<span></span>
								<span></span>
								<span></span>
								<span></span>
							</div>

						</div>

					</div>

				</section>

			</section>

		</settion>

		<section class="content">
			
			<section class="main_text" id="main_offset">
				
				<span class="title_text">Кам'янець-Подільський - прекрасне місто.</span>
				
				<span class="text">
					
					<p>Кам’янець Подільський – місто, що зберегло дух середньовіччя. Своєрідність та унікальність його полягають у гармонійному поєднанні ландшафту і містобудівної структури середньовічного міста, в якому військові інженери, використовуючи чудові природні властивості, створили фортифікаційну систему, що не має аналогів у Європі. </p></br>

					<p>До творення неповторного архітектурного вигляду Кам’янець-Подільського у різні періоди його історії доклали майстерності архітектори й скульптори з Італії, Нідерландів, Вірменії, Польщі, Франції, Туреччини та ін.</p></br>

					<p>До складу Кам’янецької фортеці входять одинадцять башт, кожна з яких має свою назву й історію.</p></br>

					<p>Так, найвища башта названа Папською тому, що була збудована на кошти, надіслані Папою Римським Юлієм II. Ще її називають Кармелюковою, бо в ній тричі був ув’язнений український народний герой Устим Кармелюк. У Чорній (кутовій) башті знаходиться криниця завглибшки 40 м і діаметром 5 м, видовбана у скелі.</p></br>

					<p>У підземеллях Замкового комплексу відкрито експозиції, що відтворюють сторінки його історії. У західному бастіоні реконструйовано панораму оборони замку 1672 р. під частурецької навали. У східному бастіоні розміщено експозицію, присвячену історії легкої метальної зброї на Поділлі, де відвідувач може вистрілити з арбалета, відчувши себе середньовічним воїном. До нашого часу збереглася система ходів і казематів.</p></br>

					<p>Сьогодні, за попередньою, домовленістю, можна стати учасником нічної театралізованої екскурсії Старою фортецею. Екскурсію у вежах (баштах) і підземеллях фортеці проводить кам’янецький «староста» та його «свита», які цікавими розповідями, піснями, танцями не лише знайомлять екскурсантів з історією замку та озброєнням, а й створюють неповторне відчуття подорожі у часі.</p></br>

					<p>Відвідувачі фортеці мають змогу поїздити верхи на конях, постріляти з арбалетів та луків, власноруч викарбувати пам’ятну монету.</p>

				</span>

			</section>

		</section>

		<section class="color_line" id="main_offset"></section>

		<?php include '../pages/footer.php'; ?>

	</section>

	<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
	<script type="text/javascript" src="js/jquery.slicebox.js"></script>
	<script type="text/javascript">
		$(function() {

			var Page = (function() {

				var $navArrows = $( '#nav-arrows' ).hide(),
					$navDots = $( '#nav-dots' ).hide(),
					$nav = $navDots.children( 'span' ),
					$shadow = $( '#shadow' ).hide(),
					slicebox = $( '#sb-slider' ).slicebox( {
						onReady : function() {

							$navArrows.show();
							$navDots.show();
							$shadow.show();

						},
						onBeforeChange : function( pos ) {

							$nav.removeClass( 'nav-dot-current' );
							$nav.eq( pos ).addClass( 'nav-dot-current' );

						}
					} ),
					
					init = function() {

						initEvents();
						
					},
					initEvents = function() {

						// add navigation events
						$navArrows.children( ':first' ).on( 'click', function() {

							slicebox.next();
							return false;

						} );

						$navArrows.children( ':last' ).on( 'click', function() {
							
							slicebox.previous();
							return false;

						} );

						$nav.each( function( i ) {
						
							$( this ).on( 'click', function( event ) {
								
								var $dot = $( this );
								
								if( !slicebox.isActive() ) {

									$nav.removeClass( 'nav-dot-current' );
									$dot.addClass( 'nav-dot-current' );
								
								}
								
								slicebox.jump( i + 1 );
								return false;
							
							} );
							
						} );

					};

					return { init : init };

			})();

			Page.init();

		});
	</script>
</body>
</html>	
